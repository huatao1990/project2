const webpackDevServer = require('webpack-dev-server')
const config = require('./webpack.dev.js')
const webpack = require('webpack')
const devserver = {
        contentBase: '../dist',
        compress: true,
        hot: true,
        open:  true,
        port: 8080,
        host: '127.0.0.1',
        stats: "minimal",
        clientLogLevel: 'warning'
}
webpackDevServer.addDevServerEntrypoints(config,devserver)
const compiler = webpack(config)
const server = new webpackDevServer(compiler,devserver)

server.listen(8080, '127.0.0.1',()=> {
    console.warn('启动成功')
    console.log(devserver.host+':'+devserver.port)
})

