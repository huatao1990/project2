const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const clearWebpackPlugin = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const webpack = require('webpack')
const vueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
    entry: {
        app: path.join(__dirname, '../src/main.js')
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.js',
            '@': path.resolve(__dirname, '../src')
        },
        extensions: ['.js', '.vue']
    },
    output: {
        filename: process.env.NODE_ENV == 'production' ? '[chunkhash].bundle.js' : '[name].bundle.js',
        // chunkFilename:'[id].[chunkhash].bundle.js',
        path: path.resolve(__dirname, '../dist')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                include: path.resolve(__dirname, '../src'),
            },
            {
                test: /\.js$/, 
                include: path.resolve(__dirname, '../src'),
                use: {
                    loader: 'babel-loader', 
                    options: {
                        cacheDirectory: true
                    },
                }
            },
            {
                test: /\.(less|css)$/,
                include: path.resolve(__dirname, '../src'),
                use: [
                    process.env.NODE_ENV == 'production' ?
                        MiniCssExtractPlugin.loader : 'vue-style-loader',
                    { loader: 'css-loader'},
                    'less-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                include: path.resolve(__dirname, '../src'),
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: true,
            minify: {
                minifyCSS: true,
                minifyJS: true
            }
        }),
        new vueLoaderPlugin(),
        new clearWebpackPlugin(['dist'], {
            root: process.cwd(),
            verbose: true,
            dry: false
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
            chunkFilename: "[id].[contenthash].css"
        }),
        new webpack.DefinePlugin({
            // 'ISWX': JSON.stringify(false)
        })
        // new ExtractTextWebapckPlugin('style.css')
    ]
}