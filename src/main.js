import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import classRouter from './router/router'
Vue.config.productionTip = false
Vue.prototype.$axios = axios
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router:classRouter,
    template: '<App/>',
    components: { App }
  })
  
